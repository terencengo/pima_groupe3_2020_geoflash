# Rapport d'itération 2
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*
 
## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*
 
|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Terence NGO              |
| **Scrum Master**        | Fabien MERCERON          |
 
## Bilan de l'itération précédente 
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Un nouveau confinement a été établi ce qui nous empêche de se voir en présentiel que cela soit lors de la démo ou bien pour les stand-ups.

> Il y a eu aussi la semaine des vacances de la Toussaint, Nicolas Mir est parti à l’étranger et n’avait pas de connexion pour se développer.
 
### Taux de complétion de l'itération 
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 10 terminés / 13 prévues = 77% 
 
### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
 
- En tant qu'utilisateur, je veux connaitre les meilleurs lieux de restauration du lieu recherché
- En tant qu'utilisateur, je veux rechercher les meilleurs hôtels du coin 
- En tant qu'utilisateur, je veux recevoir un mail de confirmation après inscription
- En tant qu'utilisateur, je veux pouvoir réinitialiser mon mdp si je l'ai perdu/oublié
- En tant qu'utilisateur, je veux pouvoir mettre à jour mon profil (nom, email, image de profile, mot de passe)
- En tant qu'utilisateur connecté je veux avoir accès à l'historique des lieux que j'ai recherché précédemment
- En tant qu'utilisateur, je veux pouvoir retirer une ville de mes favoris
- En tant qu’utilisateur je veux pouvoir ajouter une ville à mes favoris
- En tant qu'utilisateur, je veux pouvoir afficher mes lieux favoris 
- En tant qu'utilisateur je veux pouvoir filtrer les lieux culturels et touristiques par type
 
 
 
 
## Rétrospective de l'itération précédente
 ### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Nous avons rencontré quelques difficultés lors de la fusion entre une branche de travail consacrée aux favoris et la branche master
 
### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
-  Rendre les lieux favoris (uniquement ?) cliquables
-  Améliorer l’affichage des images des restaurants/lieux culturels pour éviter qu’elles soient déformées
-  Les boutons sur la map doivent rediriger non plus vers Wikipedia mais vers le search associé
-  Connecter la barre de recherche et la map d’une quelconque manière 
-  Mieux s’organiser pour les fusions de branches sur gitlab

## Prévisions de l'itération suivante 
### Évènements prévus 
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
-  Pendant le prochain sprint, il y aura toujours le confinement qui nous obligera à travailler à distance. 
-  Dans les autres matières plusieurs projets sont donnés à cette période de l’année donc nous aurons moins de temps à consacrer à ce projet.
 
### Titre des User Stories reportées 
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
-  Barre de recherche avec auto-complétion
-  En tant qu'utilisateur, je veux avoir une page qui me propose des centres d'intérêt que je puisse sélectionner
-  (A terminer) En tant qu'utilisateur, je veux pouvoir utiliser une carte dynamique pour pouvoir choisir ma ville


### Titre des nouvelles User Stories 
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
-  En tant qu'utilisateur je veux avoir accès à une belle page de recherche
-  En tant qu'utilisateur, je veux une barre de recherche avec auto-complétion
-  En tant qu'utilisateur, je veux pouvoir utiliser la barre de recherche à partir de n'importe quelle page
-  En tant qu'utilisateur, je veux pouvoir cliquer sur un bouton associé à une ville sur la map, pour obtenir les recherches associées
-  En tant qu'utilisateur je veux avoir accès à une page qui présente le projet Geoflash
-  En tant qu'utilisateur, je veux avoir une page qui me propose des centres d'intérêt que je puisse sélectionner
-  En tant qu'utilisateur je veux mettre en avant les recherches en fonction de mes centres d'intérêt
-  En tant qu'utilisateur je veux pouvoir accéder à une magnifique page de profil
-  En tant qu'utilisateur je veux avoir accès à mes villes favoris (en rouge, les villes par défaut étant en bleue) sur la carte dynamique
-  En tant qu'utilisateur, je veux pouvoir accéder à de jolis graphes en cliquant sur une ville de la map


## Confiance
### Taux de confiance de l'équipe dans l'itération 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*
 
|           | :(    | :|   | :)    | :D    |
|:--------: |:----: |:----:|:----: |:----: |
| Equipe 7  |  *0*  |  *1* |  *5*  |  *0*  |
 
### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*
 
|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 7  |  *0*  |  *0*      |  *3*  |  *3*  |
 
 