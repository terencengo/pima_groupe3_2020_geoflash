# Rapport d'itération 4
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                   | Itération précédente    |
| -------------             |    -------------        |
| **** Product Owner        | **   Fabien Merceron    |
| **Scrum Master   **       | **   Aymeric Keyewa     |

## Bilan de l'itération précédente 
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.* 
Rendus de projets en tout genre 

### Taux de complétion de l'itération 
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
11/13 (site en français à moitié & pop up qui redirige vers une autre page + 1 carte bonus javascript non faite)

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
 
En tant qu’utilisateur, je veux une jolie présentation du résultat des vol
 
En tant qu’utilisateur, je veux un joli site
 
En tant qu’utilisateur, je veux pouvoir accéder à une page avec la liste de mes centres d’intérêt
 
En tant qu’utilisateur, je veux une page de profil esthétique
 
En tant qu’utilisateur, je veux avoir accès à un panel de villes sur la carte sans que cela ralentisse le temps de chargement de cette page
 
En tant qu’utilisateur, je veux pouvoir ajouter une ville en favori sans changer de page depuis la page de recherche
 
En tant qu’utilisateur, je veux pouvoir entrer n’importe quelle ville dans la barre de recherche sans que cela cause un bug

En tant qu’utilisateur, je veux, lors de la planification d’un vol, qu’on me propose des noms de villes et d’aéroports
 
En tant qu’utilisateur, je veux pouvoir supprimer une ville depuis la page des favoris
 
En tant qu’utilisateur, je veux pouvoir supprimer un centre d’intérêt depuis la page des centres d’intérêts
 
En tant qu’utilisateur, je veux pouvoir “grap” la map sans que la barre de navigation ne disparaisse
  
## Rétrospective de l'itération précédente
 ### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
 
Rendre le code très lisible et commenté a été très  utile pour corriger les bugs rapidement et efficacement
 
On s’est concentré sur le débug car il y avait pas mal de bugs pour couvrir le maximum de cas de tests

### Axes d'améliorations pour une éventuelle continuation du projet
*Quels sont les axes d'améliorations pour une éventuelle suite du projet?*
 
Réduire le temps de chargement de la page de la carte
 
Ajouter du code javascript pour ne pas avoir à ouvrir plusieurs pages lors de certaines actions: par exemple lorsqu’on cherche les informations d’une ville depuis la carte
 
Pouvoir sélectionner des vols aller-retours dans “Voyager”


## Confiance
### Taux de confiance de l'équipe dans l'itération 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 7  |  *0*  |  *0*      |  *6*  |  *0*  |

### Taux de confiance de l'équipe pour la réalisation du projet

*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 7  |  *0*  |  *0*      |  *0*  |  *6*  |