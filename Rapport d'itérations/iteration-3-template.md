# Rapport d'itération 3
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                   | Itération précédente    |
| -------------             |    -------------        |
| **** Product Owner        | **   Rathea UTH          |
| **Scrum Master   **       | **   Loïc MARY           |

## Bilan de l'itération précédente 
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.* 

>Nous avons eu un problème de connexion au serveur

>Nous avons dû faire face à quelques problèmes de merge entre la branche master et intérêt, mais ils ont été résolus.

### Taux de complétion de l'itération 
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> 10 terminées / 10 prévues = 100%

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
-  En tant qu'utilisateur, je veux pouvoir cliquer sur un bouton associé à une ville sur la map, pour obtenir les recherches associées
-  En tant qu'utilisateur je veux avoir accès à mes villes favoris (en rouge, les villes par défaut étant en bleue) sur la carte dynamique
-  En tant qu'utilisateur je veux avoir accès à une page (à propos) qui présente le projet Geoflash
-  En tant qu'utilisateur je veux mettre en avant les recherches en fonction de mes centres d'intérêt
-  En tant qu'utilisateur je veux avoir accès à une belle page de recherche
-  En tant qu'utilisateur, je veux pouvoir utiliser la barre de recherche à partir de n'importe quelle page
-  Barre de recherche avec auto-complétion
-  En tant qu'utilisateur, je veux pouvoir accéder à des informations sur des vols à venir
-  En tant qu'utilisateur je veux pouvoir accéder à une magnifique page de profil
-  En tant qu'utilisateur, je veux avoir une page qui me propose des centres d'intérêt que je puisse sélectionner
  
## Rétrospective de l'itération précédente
 ### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
-  On ne peut pas accéder aux centres d’intérêts sans en ajouter
-  On ne peut pas ajouter trop de villes sur la carte dynamique sans que ça prenne trop de temps pour charger
-  Il serait préférable de mettre tout le site en français

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
> On va se concentrer sur le CSS et le débug pour que la démo finale se passe au mieux
 
### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Rendre le code plus lisible pour une relecture simplifiée

## Prévisions de l'itération suivante 
### Évènements prévus 
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Les autres projets dont les deadlines approchent également

### Titre des User Stories reportées 

> Pas de User Story reportée

### Titre des nouvelles User Stories   

-  En tant qu’utilisateur, je veux, lors de la planification d’un vol, qu’on me propose des noms de villes et d’aéroports
-  En tant qu’utilisateur, je veux que le site soit dans une seule langue: le français
-  En tant qu’utilisateur, je veux une page de profil esthétique
-  En tant qu’utilisateur, je veux une jolie présentation du résultat des vols
-  En tant qu’utilisateur, je veux avoir accès à un panel de villes sur la carte sans que cela ralentisse le temps de chargement de cette page
-  En tant qu’utilisateur, je veux pouvoir entrer n’importe quelle ville dans la barre de recherche sans que cela cause un bug
-  En tant qu’utilisateur, je veux pouvoir “grap” la map sans que la barre de navigation ne disparaisse
-  En tant qu’utilisateur, je veux un joli site
-  En tant qu’utilisateur, je veux pouvoir accéder à une page avec la liste de mes centres d’intérêt 
-  En tant qu’utilisateur, je veux animer le site 
-  En tant qu’utilisateur, je veux pouvoir ajouter une ville en favori sans changer de page depuis la page de recherche 
-  En tant qu’utilisateur, je veux pouvoir supprimer une ville depuis la page des favoris 
-  En tant qu’utilisateur, je veux pouvoir supprimer un centre d’intérêt depuis la page des centres d’intérêts 


## Confiance
### Taux de confiance de l'équipe dans l'itération 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 7  |  *0*  |  *0*      |  *6*  |  *0*  |

### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 7  |  *0*  |  *0*      |  *0*  |  *6*  |