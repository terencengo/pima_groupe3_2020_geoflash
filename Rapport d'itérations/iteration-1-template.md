# Rapport d'itération 
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*
 
## Composition de l'équipe
*Remplir ce tableau avec la composition de l'équipe et les rôles.*
 
|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Aymeric Keyewa           |
| **Scrum Master**        | Nicolas Mir              |
 
## Bilan de l'itération précédente 
### Évènements
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> Un des membres de notre groupe a été testé positif au coronavirus. De ce fait, cela a eu une influence sur la communication au sein de notre groupe car nous ne pouvions plus nous voir en présentiel. Nous avons opté pour une communication avec l'outil Discord.
 
### Taux de complétion de l'itération 
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
 
> 8 terminés / 8 prévues = 100%
 
### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
- En tant qu’utilisateur, je veux pouvoir me créer un compte
- En tant qu’utilisateur, je veux pouvoir m'authentifier à mon compte
- En tant qu’utilisateur, je veux pouvoir me déconnecter de mon compte
- En tant qu'utilisateur, je veux pouvoir collecter des photos de la ville recherchée
- En tant qu’utilisateur, je veux avoir accès aux différentes catégories une fois arrivé sur la page d’une ville.
- En tant qu'utilisateur, je veux pouvoir entrer le nom d'un lieu dans une barre de recherche puis accéder à la page donnant les infos sur celui-ci 
- En tant qu'utilisateur, je veux connaitre les lieux culturels et touristiques du lieu recherché 
## Rétrospective de l'itération précédente
 ### Bilans des retours et des précédentes actions
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
 
> Pour la plupart des membres de notre groupe Django et les APIs étaient une découverte. Donc avant de coder on est tous passé par une phase de recherche et d’installation des modules ce qui a ralenti notre avancée dans la progression des User Stories. Mais le fait que l’on anticipe sur cette phase en donnant des complexités plus élevées aux User Stories qui nous semblaient plus obscures nous a permi de passer plus de temps pour faire nos choix de développement.
 
> Nous avons su être agiles en comprenant les besoins de chaque membre de l’équipe et en aidant nos camarades, notamment au niveau des API qui se ressemblent pas mal pour la plupart.
 
> Nous avons toujours réfléchi en termes de fonctionnalité pour l’utilisateur: nous avons bien géré les priorités liées aux besoins premiers du client.
 
 
### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
> Nous allons créer des branches en parallèle de Master pour mieux gérer les ajouts de code.

> Commencer la fonctionnalité des lieux favoris 

 
### Axes d'améliorations
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> Les Dev traiterons des cas particuliers et exceptions: par exemple, les lieux touristiques affichés lorsqu’on entre “Evry” se trouvent aux USA; il faut absolument entrer “Évry” pour qu’il n’y ai pas de problèmes.

> Mettre une carte dynamique

> Proposer des informations supplémentaires pour chaque lieu touristique

 
## Prévisions de l'itération suivante 
### Évènements prévus 
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Il y a une semaine de vacances à la Toussaint

> La situation avec le covid risque de se détériorer entraînant des changements dans l’organisation de l’école et de ses cours
 
### Titre des User Stories reportées 
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
 
 
### Titre des nouvelles User Stories 
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
-  En tant qu’utilisateur connecté je veux pouvoir ajouter une ville à mes favoris
-  En tant qu’utilisateur connecté je veux pouvoir retirer une ville de mes favoris
-  En tant qu'utilisateur connecté je veux avoir accès à l'historique des lieux que j'ai recherché précédemment
-  En tant qu'utilisateur, je veux pouvoir utiliser une carte dynamique pour pouvoir choisir ma ville
-  En tant qu'utilisateur, je veux pouvoir afficher mes lieux favoris 
-  En tant qu'utilisateur, je veux une barre de recherche avec auto-complétion 
-  En tant qu'utilisateur, je veux avoir une page qui me propose des centres d'intérêt que je puisse sélectionner
-  En tant qu'utilisateur, je veux rechercher les meilleurs hôtels du coin 
-  En tant qu'utilisateur, je veux connaitre les meilleurs lieux de restauration du lieu recherché 
-  En tant qu'utilisateur, je veux pouvoir mettre à jour mon profil 
-  En tant qu'utilisateur, je veux pouvoir réinitialiser mon mdp si je l'ai perdu/oublié 
 
## Confiance
### Taux de confiance de l'équipe dans l'itération 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*
 
|           | :(    | :|        | :)    | :D    |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 3  |  *0*  |  *0*      |  *4*  |  *2*  |
 
### Taux de confiance de l'équipe pour la réalisation du projet
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*
 
|           | :(    | :|        | :)    | :D|   |
|:--------: |:----: |:----:     |:----: |:----: |
| Equipe 3  |  *0*  |  *0*      |  *0*  |  *6*  |

 
 