from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
import wikipedia
from django.template.defaulttags import register
import os
from .models import Ville
# Module pour obtenir le code source d'une page avant de le parser
import urllib
from django.urls import reverse_lazy, reverse
# Modules pour parser des HTMLs
import requests
import json
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import googlemaps
import pprint
import time
import folium
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from users.models import Interest
from django.contrib import messages

#register = template.Library()

import re
import numpy as np
from amadeus import Client, ResponseError
import pandas as pd

import goslate


# Pour faire des boucles "for" dans HTML
@register.filter(name='range')
def filter_range(start, end):
  return range(start, end)


wikipedia.set_lang("fr")

location = ""

#coordinates = {
                # France
                #"Paris": [48.8534, 2.3488],
                #"Marseille": [43.296482, 5.36978],
                #"Lyon": [45.75, 4.85],
                #"Nice": [43.7,  7.25],
                #"Nantes": [47.218371, -1.553621],
                #"Montpellier": [43.6, 3.8833],
                #"Strasbourg": [48.5833, 7.75],
                #"Bordeaux": [44.8333, -0.5667],
                #"Toulouse": [43.604652, 1.444209],
                #"Lille": [50.6333, 3.0667],

                # Angleterre
                #"Londres": [51.5073509, -0.1277583],

#} 

villes_defaut=["Paris","Marseille","Lyon","Nice","Nantes","Montpellier","Strasbourg",
                "Bordeaux","Toulouse","Lille","Londres",
               "Madrid","Manchester","Buenos Aires","Evry","Dunkerque","Rome","Amsterdam","New-York","Moscou","Bombay","Milan","Alger",
               "Lisbonne","Sydney","Pekin","Tokyo","Berlin","Kinshasa","Johannesbourg","Athenes","Montreal","Sao Paulo"]


dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, 'liste-197-etats-2020.csv')

df = pd.read_csv(filename,sep=';',encoding='latin-1')
capitales = list(df.CAPITALE) 

# define a gmaps with API key
gmaps = googlemaps.Client(key = 'AIzaSyCcs4XWpWZQnpWRWEPWTYtQSvH5Km-CjM8')

# a function we use to retreive the coordinates from a list of cities
def get_coordinates(ville):
    list_coordinates = []
    for i in range(len(ville)):
        coordinates = gmaps.geocode(address=ville[i])
        #Get coord details (latitute, longitude)
        get_lat_lon = [coordinates[0]['geometry']['location']['lat'],coordinates[0]['geometry']['location']['lng']]
        list_lat_lon = list(np.float_(get_lat_lon))
        list_coordinates.append(list_lat_lon)
    return list_coordinates

def welcome(request):
    global capitales
    #city_names = list(coordinates.keys())
    city_names=[str(x) for x in capitales]
    
    if request.user.is_authenticated:  
        # get a list of favourite cities from database
        ville_favourite = list(Ville.objects.filter(favourite=request.user))
        # change it to a list of string
        villes_fav = [str(x) for x in ville_favourite] 
        # get its coordinates list
        coord_villes_fav = get_coordinates(villes_fav) 

        # remove city names which are in list of favourite villes (just in case)
        city_names = list(np.setdiff1d(city_names,villes_fav)) 

    if request.POST.get("map_theme") == "Stamen Toner":
        m = folium.Map(location = [48.8534, 2.3488], zoom_start=6, max_zoom=16, min_zoom=2, tiles="Stamen Toner")
    elif request.POST.get("map_theme") == "Stamen Terrain":
        m = folium.Map(location = [48.8534, 2.3488], zoom_start=6, max_zoom=16, min_zoom=2, tiles="Stamen Terrain")
    elif request.POST.get("map_theme") ==  "CartoDB positron":
        m = folium.Map(location = [48.8534, 2.3488], zoom_start=6, max_zoom=16, min_zoom=2, tiles="CartoDB positron")
    elif request.POST.get("map_theme") == "Cartodbdark_matter":
        m = folium.Map(location = [48.8534, 2.3488], zoom_start=6, max_zoom=16, min_zoom=2, tiles="Cartodbdark_matter")
    else:
        m = folium.Map(location = [48.8534, 2.3488], zoom_start=6, max_zoom=16, min_zoom=2, tiles="OpenStreetMap")
    

    coord_villes_defaut=get_coordinates(city_names) 
    for i in range(len(city_names)):



        #http://localhost:8000/welcome/geoflash/recherche/?csrfmiddlewaretoken=VRnJ0UObpLQA0ddGRGIHZFpsrV4CAfFDaHr2hLhWfqLNm4Mo7M3B2BrRBDqPztGq&recherche=

        #http://127.0.0.1:8000/welcome/geoflash/recherche/?csrfmiddlewaretoken=VRnJ0UObpLQA0ddGRGIHZFpsrV4CAfFDaHr2hLhWfqLNm4Mo7M3B2BrRBDqPztGq&recherche=

        popup = folium.Popup('<a href="http://127.0.0.1:8000/welcome/geoflash/recherche/?csrfmiddlewaretoken=GpWFOuWYLgDQKKEuJ9PIgdNUnUT34ANryCCmzo0K1NXVjaI1J0bsQDO4htihhv6n&recherche='+city_names[i]+'",name="popup" target="_blank">Informations</button>')

        folium.Marker(location= coord_villes_defaut[i] , popup=popup, tooltip=city_names[i]).add_to(m)


    if request.user.is_authenticated:
        
        for j in range(len(villes_fav)):
        
            popup = folium.Popup('<a href="http://127.0.0.1:8000/geoflash/recherche/?csrfmiddlewaretoken=o7ZSwnr1gMEOjzSyw8tcSkPlAv4MON1yNvndB5RzvBwH3WvWIScjji2BQCUL3mPc&recherche='+villes_fav[j]+'",name="popup" target="_blank">Informations</button>')
            folium.Marker(location= coord_villes_fav[j], popup=popup, tooltip='Your favourite city: ' + villes_fav[j], icon=folium.Icon(color='red', icon='map-marker')).add_to(m)

    


    m = m._repr_html_()
    
    context = {
        'map' : m,
    }
    return render(request, 'welcome.html', context)

    #'<a href="https://fr.wikipedia.org/wiki/' + city_names[i] + '"

"""
def favoris(request):
    return render(request, 'favoris.html')
"""
@login_required
def favoris(request):
    new=Ville.objects.filter(favourite=request.user)
    messages.success(request, 'Successfully Saved')
    return render(request,"favoris.html",{'new':new})

def recherche(request):
    entered = str(request.GET.get("recherche"))
    global location
    location = entered

    liste_info = location.split(',') #récupère la ville et le pays a priori
    location = liste_info[0].strip()

    if (" - " in location):
        location = location.split(' - ')[0].strip()

    #location = wikipedia.search(location, results=3)[0]
    #url = wikipedia.page(location)

    #Permet d'ajouter la ville à la base de données
    ville=Ville.objects.create(name=location)
    #Permet que l'étoile soit pleine si ville est favorie et vide sinon
    ville=Ville.objects.filter(name=location)[0]
    user = User.objects.filter(id=request.user.id)[0]
    is_favourite=False
    if ville.favourite.filter(id=request.user.id):
        is_favourite=True
    #Permet d'ajouter à l'historique de l'utilisateur
    user = User.objects.filter(id=request.user.id)[0]
    ville.historique.add(user)
    
    
    url = 'https://fr.wikipedia.org/wiki/'+location
    url2 = 'https://fr.wikipedia.org/wiki/'+location+"_(ville)" #il est possible que le format de la page wikipédia change

    req = requests.get(url)
    req2 = requests.get(url2)

    soup = BeautifulSoup(req.content, 'html.parser')
    soup2 = BeautifulSoup(req2.content, 'html.parser')

    long_string = False #know if the location name was modified

    try:
        html_data = soup.find("table", {"class" : "infobox_v2"}) # les villes qui ont été testé ont toutes cette classe qui regroupe les informations générales
        html_data = html_data.find("tbody") # on récupère le tableau
        infobox = 2
    except:
        try:
            html_data = soup2.find("table", {"class" : "infobox_v2"})
            html_data = html_data.find("tbody")
            location = location+"_(ville)"
            infobox = 2
            long_string = True
        except:
            try:
                html_data = soup.find("div", {"class" : "infobox_v3"})
                infobox = 3
            except:
                try:
                    html_data = soup2.find("div", {"class" : "infobox_v3"})
                    location = location+"_(ville)"
                    long_string = True
                    infobox = 3
                except:
                    context = {
                        "recherche": location
                    }
                    return render(request, 'invalide-recherche.html', context)

    table_data = {} #on va remplir un dictionnaire et on initialise toutes les données comme étant inconnues
    table_data["Pays"] = "Inconnu"
    table_data["Site_web"] = "Inconnu"
    table_data["Population"] = "Inconnu"
    table_data["Population_type"] = "Inconnu"
    table_data["Maire"] = "Inconnu"
    table_data["Superficie"] = "Inconnu"
    table_data["Superficie_unite"] = "Inconnu"

    has_country = False #on va filtrer les villes des autres données
    has_population = False
    has_maire = False
    has_area = False

    if html_data is not None:
        for row in html_data.find_all('tr'):
            header = row.find('th')
            data = row.find('td')

            if (header is not None):
                header = header.getText().strip()

                if (header == "Pays"):
                    if (data is not None):
                        data = data.getText()
                        table_data["Pays"] = data.strip()
                        has_country = True

                if (header == "Site web"):
                    if (data is not None):
                        data = data.getText()
                        table_data["Site_web"] = data.strip()

                if (infobox == 2):
                    if (header == "Population" or header == "Populationmunicipale" or header == "Population de l'agglomération"):
                        if (data is not None):
                            data = [text for text in data.stripped_strings]
                            table_data["Population"] = data[0].strip()+" "+data[1]
                            table_data["Population_type"] = header
                            has_population = True
                if (infobox == 3):
                    if (header == "Population"):
                        if (data is not None):
                            data = [text for text in data.stripped_strings]
                            table_data["Population"] = data[0].strip()
                            table_data["Population_type"] = header
                            has_population = True

                if (header == "Maire Mandat" or header == "Maire délégué" or header == "Maire"):
                    if (data is not None):
                        data = [text for text in data.stripped_strings]
                        table_data["Maire"] = data[0].replace("(","").strip()
                        has_maire = True

                if (header == "Superficie"):
                    if (data is not None):
                        data = [text for text in data.stripped_strings]
                        table_data["Superficie"] = data[0].strip()
                        table_data["Superficie_unite"] = data[1].strip()
                        has_area = True

    if (has_country): #on détermine si la donnée est une ville
        is_city = True
    else:
        is_city = False

    if not(is_city):
       return render(request, 'invalide-recherche.html')

    intro = wikipedia.summary(location, auto_suggest=False, sentences=3)
    main_img = wikipedia.WikipediaPage(title=location).images[0]

    if long_string:
        location = location.replace("_(ville)","")

    context = {
        "recherche": location,
        "intro": intro,
        "main_img_url": main_img,
        "pays": table_data["Pays"],
        "maire": table_data["Maire"],
        "site_web": table_data["Site_web"],
        "population": table_data["Population"],
        "type_population": table_data["Population_type"],
        "superficie": table_data["Superficie"],
        "superficie_unite": table_data["Superficie_unite"],
        "is_favourite":is_favourite
    }

    return render(request, 'recherche.html', context)

def request(request):
    return render(request, "request.html")

def galerie(request):
    
    entered = location
    r=requests.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyC5ydjtb75gjj6Omtmj-XaFWyATDNd7qY4&cx=c1eba54a26126a82e&q='+location+'&searchType=image')
    context = {
        "main_img_url0": json.loads(r.text)['items'][0]['link'],
        "main_img_url1": json.loads(r.text)['items'][1]['link'], 
        "main_img_url2": json.loads(r.text)['items'][2]['link'],
        "main_img_url3": json.loads(r.text)['items'][3]['link'],
        "main_img_url4": json.loads(r.text)['items'][4]['link'],
        "main_img_url5": json.loads(r.text)['items'][5]['link'],
        
    }
    
    return render(request, "galerie.html", context)

"""def visiter(request):
    # 1) Get the location searched
    search = location

    # 2) Get the connexion with Google Chrome
    chrome_driver_binary = '/Users/mirnicolas/chromedriver'
    driver = webdriver.Chrome(chrome_driver_binary)
    driver.get("https://www.google.com")

    # 3) Enter the search
    input_elem = driver.find_element_by_css_selector('input[name=q]')
    input_elem.send_keys(search)
    input_elem.send_keys(Keys.ENTER)

    # 4) Parse the resulted page
    soup = BeautifulSoup(driver.page_source, "html.parser")
    lieux = soup.findAll("span", {"class" : "aVSTQd oz3cqf rOVRL"})

    # 5) Store the information in content
    content = {"search": search}
    for i in range(6):
        content["lieu" + str(i+1)] = str(lieux[i].text)
    
    # 6) Exit 
    driver.quit()
    return render(request, "visiter.html", content)"""


def get_places_details(query,list_loc_types,coord_str,my_fields,separate,name_key):
    #Define API Key
    API_KEY = 'AIzaSyCcs4XWpWZQnpWRWEPWTYtQSvH5Km-CjM8'

    #Define client
    gmaps = googlemaps.Client(key = API_KEY)

    context= {"search" : query}
    if separate:

        for loc_type in list_loc_types:

            places_result = gmaps.places_nearby(location=coord_str ,radius=10000,language="FR",type=loc_type)

            #pprint.pprint(places_result) #print json results for tests
            #print(places_result)

            #Loop through the results and add them to content

            content = {}
            i = 0
            for place in places_result['results']:

                #define place id
                my_place_id = place['place_id']


                #make a request for the details
                place_details=gmaps.place(place_id = my_place_id,language="FR", fields = my_fields)

                #print the results
                #print(place_details)

                #add results to content
                #content["lieu" + str(i+1)] = str(place_details['result']['name']) + '; ' + str(place_details['result']['formatted_address'])

                #check if place has rating

                if 'rating' in place_details['result'].keys():
                    rating = str(place_details['result']['rating']) + "/5" 
                else:
                    rating = "N'a pas encore reçu de note"

                
                #check if place has photos

                if 'photos' in place_details['result'].keys():
                    link_img = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidth=700&photoreference=" + str(place_details['result']['photos'][0]['photo_reference']) + "&key=" + API_KEY
                else:
                    link_img = "/images/default_no_image.jpg"

                #store each place details with a dict
                content["lieu" + str(i+1)] = {"nom": str(place_details['result']['name']), "adresse": str(place_details['result']['formatted_address']), "note": rating , "img": link_img}  
                i+=1    

            context[loc_type] = content

    else: #We put all locations at a same place
        i = 0
        for loc_type in list_loc_types:

            places_result = gmaps.places_nearby(location=coord_str ,radius=10000,language="FR",type=loc_type)

            #pprint.pprint(places_result) #print json results for tests
            #print(places_result)

            #Loop through the results and add them to content

            content = {}
            
            for place in places_result['results']:

                #define place id
                my_place_id = place['place_id']


                #make a request for the details
                place_details=gmaps.place(place_id = my_place_id,language="FR", fields = my_fields)

                #print the results
                #print(place_details)

                #add results to content
                #content["lieu" + str(i+1)] = str(place_details['result']['name']) + '; ' + str(place_details['result']['formatted_address'])

                #check if place has rating

                if 'rating' in place_details['result'].keys():
                    rating = str(place_details['result']['rating']) + "/5" 
                else:
                    rating = "N'a pas encore reçu de note"

                
                #check if place has photos

                if 'photos' in place_details['result'].keys():
                    link_img = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidth=700&photoreference=" + str(place_details['result']['photos'][0]['photo_reference']) + "&key=" + API_KEY
                else:
                    link_img = "/images/default_no_image.jpg"

                #store each place details with a dict
                content["lieu" + str(i+1)] = {"nom": str(place_details['result']['name']), "adresse": str(place_details['result']['formatted_address']), "note": rating , "img": link_img}  
                i+=1    

            context[name_key] = content

    return context


def visiter(request):

    #Define API Key
    API_KEY = 'AIzaSyCcs4XWpWZQnpWRWEPWTYtQSvH5Km-CjM8'

    #Define client
    gmaps = googlemaps.Client(key = API_KEY)

    #Define querry
    query = location

    #Get location infos from query
    coordinates=gmaps.geocode(address=query)

    # Ex result : [{'address_components': [{'long_name': 'Paris', 'short_name': 'Paris', 'types': ['locality', 'political']}, {'long_name': 'Paris', 'short_name': 'Paris', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Île-de-France', 'short_name': 'IDF', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'France', 'short_name': 'FR', 'types': ['country', 'political']}], 'formatted_address': 'Paris, France', 'geometry': {'bounds': {'northeast': {'lat': 48.9021449, 'lng': 2.4699208}, 'southwest': {'lat': 48.815573, 'lng': 2.224199}}, 'location': {'lat': 48.856614, 'lng': 2.3522219}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 48.9021449, 'lng': 2.4699208}, 'southwest': {'lat': 48.815573, 'lng': 2.224199}}}, 'place_id': 'ChIJD7fiBh9u5kcRYJSMaMOCCwQ', 'types': ['locality', 'political']}]

    #Get coord details
    coord_str=str(coordinates[0]['geometry']['location'] ['lat']) + ',' + str(coordinates[0]['geometry']['location'] ['lng'])

    interests = Interest.objects.filter(user=request.user.id)

    #Define the type of location we want
    list_loc_types=['museum','lodging','amusement_park','aquarium','art_gallery']

    #define the fields we want to get as place details
    my_fields = ['name', 'type', 'formatted_address','rating','photo']

    context1 = get_places_details(query,interests,coord_str,my_fields,False,'interests')

    context2 = get_places_details(query,list_loc_types,coord_str,my_fields,True,'nada')

    context={**context1, **context2}

    if len(context1) == 1:
        context["visibility"] = "hidden"
    else:
        context["visibility"] = "visible"

    return render(request, "visiter.html", context)


def restauration(request):

    #Define API Key
    API_KEY = 'AIzaSyCcs4XWpWZQnpWRWEPWTYtQSvH5Km-CjM8'

    #Define client
    gmaps = googlemaps.Client(key = API_KEY)

    #Define querry
    query = location

    #Get location infos from query
    coordinates=gmaps.geocode(address=query)

    # Ex result : [{'address_components': [{'long_name': 'Paris', 'short_name': 'Paris', 'types': ['locality', 'political']}, {'long_name': 'Paris', 'short_name': 'Paris', 'types': ['administrative_area_level_2', 'political']}, {'long_name': 'Île-de-France', 'short_name': 'IDF', 'types': ['administrative_area_level_1', 'political']}, {'long_name': 'France', 'short_name': 'FR', 'types': ['country', 'political']}], 'formatted_address': 'Paris, France', 'geometry': {'bounds': {'northeast': {'lat': 48.9021449, 'lng': 2.4699208}, 'southwest': {'lat': 48.815573, 'lng': 2.224199}}, 'location': {'lat': 48.856614, 'lng': 2.3522219}, 'location_type': 'APPROXIMATE', 'viewport': {'northeast': {'lat': 48.9021449, 'lng': 2.4699208}, 'southwest': {'lat': 48.815573, 'lng': 2.224199}}}, 'place_id': 'ChIJD7fiBh9u5kcRYJSMaMOCCwQ', 'types': ['locality', 'political']}]

    #Get coord details
    coord_str=str(coordinates[0]['geometry']['location'] ['lat']) + ',' + str(coordinates[0]['geometry']['location'] ['lng'])

    #Define the type of location we want
    list_loc_types=['restaurant']

    my_fields = ['name', 'type', 'formatted_address','rating','photo']  

    #pprint.pprint(places_result) #print json results for tests
    #print(places_result)

    #Loop through the results and add them to content

    context = get_places_details(query,list_loc_types,coord_str,my_fields, separate=True, name_key="eazfjz")

    return render(request, "restauration.html", context)





#Ajout aux favoris
@login_required
def ville_fav(request,id):
    user = User.objects.filter(id=request.user.id)[0]
    #ville=Ville.objects.create(name=request.GET['add_fav_btn'])
    if 'add_fav_btn' in request.GET:
        ville = Ville.objects.filter(name=request.GET['add_fav_btn'])[0]
        if ville.favourite.filter(id=request.user.id) :
            ville.favourite.remove(user)
        else :          
            ville.favourite.add(user)
        ville.save()
        
    else:
        ville=False
    content = {
        'user':user,
        'ville':ville,
        }
    new=Ville.objects.filter(favourite=request.user)
    
    
    return render(request,"favoris.html",{'new':new})

@login_required
def delete_ville(request,id):
    user = User.objects.filter(id=request.user.id)[0]
    if 'delete_fav_btn' in request.GET:
        ville = Ville.objects.filter(name=request.GET['delete_fav_btn'])[0]
        if ville.favourite.filter(id=request.user.id) :
            ville.favourite.remove(user)
        ville.save()
    else:
        ville=False
    new=Ville.objects.filter(favourite=request.user)

    return render(request,"favoris.html",{'new':new})

@login_required
def delete_int(request,id):
    user = User.objects.filter(id=request.user.id)[0]
    if 'delete_int_btn' in request.GET:
        interest = Interest.objects.filter(name=request.GET['delete_int_btn'])[0]
        if interest.user.filter(id=request.user.id) :
            interest.user.remove(user)
        interest.save()
    else:
        interest=False
    centres_int=Interest.objects.filter(user=request.user.id)
    return render(request,"interest.html",{'centres_int':centres_int})

@login_required
def favourite_list(request):
    new=Ville.objects.filter(favourite=request.user)
    return render(request,"favoris.html",{'new':new})

@login_required
def interest_list(request):
    centres_int=Interest.objects.filter(user=request.user.id)
    return render(request,"interest.html",{'centres_int':centres_int})

@login_required
def historique_list(request):
    histo=Ville.objects.filter(historique=request.user)
    return render(request,"historique.html",{'histo':histo})

def retour_accueil(request):
    if 'retour_accueil' in request.GET:
        return render(request, "welcome.html")

def retour_favoris(request):
    if 'retour_favoris' in request.GET:
        new=Ville.objects.filter(favourite=request.user)
        return render(request, "favoris.html",{'new':new})

def retour_int(request):
    if 'retour_int' in request.GET:
        centres_int=Interest.objects.filter(user=request.user.id)
        return render(request,"interest.html",{'centres_int':centres_int})



# AMADEUS CONNEXION
amadeus = Client(
    client_id='jCkDgA7vGEohAnMr5tAwEd8Y3AU20GuT',
    client_secret='oLzuVuc2q2xeHX1A'
)

# Importation et restructuration des données aériens pour travel
# ICAO CODE (AEROPORTS)
icao_infos = pd.read_csv("static/data/icao.csv").iloc[:,[1,3,4]]
col_names = ["airport name", "Country", "code"]
icao_infos.columns = col_names
airport_names = np.array(icao_infos.iloc[:,0])

# Pour l'instant, il y a des répétitions dans countries
countries = np.array(icao_infos.iloc[:,1])

# Mtn valeurs uniques
#translator = Translator()
countries_sorted = sorted(list(set(countries)))

codes = np.array(icao_infos.iloc[:,2])

nb_aeroports = len(airport_names)

def get_all_airports(location_name):
  l = []
  for i in range(nb_aeroports):
    if re.search(location_name, countries[i], re.IGNORECASE):
      l.append(airport_names[i])
  return l

def get_icao(aeroport_name):
  icao = "not found"
  found = False
  for i in range(nb_aeroports):
    if re.search(aeroport_name, airport_names[i], re.IGNORECASE):
      found = True
      break
  if found:
    icao = codes[i]
  return icao

# IATA CODE (AGENCES AERIENNES)
iata = pd.read_csv("static/data/iata.csv").iloc[:,[1,3]]
agencies = iata["Name"]
iata_codes = iata["IATA"]
n_agencies = len(agencies)

def get_agency(iata_code):
  agency = "not found"
  found = False
  for i in range(n_agencies):
    # type(iata_codes[i]) != float => iata_codes[i] n'est pas NaN
    if type(iata_codes[i]) != float:
      if re.search(iata_code, iata_codes[i], re.IGNORECASE):
        found = True
        break
  if found:
    agency = agencies[i]
  return agency

# From "2020-11-21T20:55:00" to '21/11/2020 at 20:55:00'
def get_time(s):
  tmp = s.split('T')
  hour = tmp[1]
  date = tmp[0].split('-')[2] + '/'+ tmp[0].split('-')[1] + '/' +tmp[0].split('-')[0]
  hour_elts = hour.split(':')
  hour = hour_elts[0] + 'h' + hour_elts[1]
  return date + ' à ' + hour

def get_date(s):
  date = s.split('-')[2] + '/'+ s.split('-')[1] + '/' +s.split('-')[0]
  return date 


# Fonction principale pour rechercher vols
def get_flights(location_depart,
                location_arrive,
                dateDepart,
                n_adults
                ):
  depart_iata = get_icao(location_depart)
  arrive_iata = get_icao(location_arrive)
  try:
      # Recherche des vols associés aux critères
      res = amadeus.shopping.flight_offers_search.get(originLocationCode=depart_iata,
                                              destinationLocationCode=arrive_iata,
                                              departureDate=dateDepart,
                                              adults=n_adults,
                                              )
      
      vols = []
      n_propositions = len(res.data)
      for vol in range(n_propositions):
        # On parse chaque vol trouvé
        oneWay = "Yes"
        if res.data[vol]["oneWay"]:
          oneWay = "Non"
        else:
            oneWay = "Oui"
        vol_infos = {'Price of the ticket(s)': str(res.data[vol]["price"]["total"]) + " " + res.data[vol]["price"]["currency"], 
                'Agency': get_agency(res.data[vol]["itineraries"][0]['segments'][0]['operating']['carrierCode']),
                'One way': oneWay,
                'Departure': get_time(res.data[vol]["itineraries"][0]['segments'][0]["departure"]["at"]),
                'Arrival': get_time(res.data[vol]["itineraries"][0]['segments'][0]["arrival"]["at"]),
                }
        vols.append(vol_infos)
      return vols
  except ResponseError as error:
      return []


def travel(request):
    context = {'countries': countries_sorted}
    return render(request, "travel.html", context)

country_departure = ""
country_arrival = ""

def travel_tmp(request):
    pays_depart = str(request.POST["pays_depart"])
    pays_arrive = str(request.POST["pays_arrive"])
    aeroports_depart = sorted(get_all_airports(pays_depart))
    aeroports_arrive = sorted(get_all_airports(pays_arrive))

    global country_arrival
    global country_departure
    country_departure = pays_depart
    country_arrival = pays_arrive
    context = {
        'aeroports_depart': aeroports_depart,
        'aeroports_arrive': aeroports_arrive,
        'pays_depart': pays_depart,
        'pays_arrive': pays_arrive,
    }
    return render(request, "travel_tmp.html", context)

def travel_no(request):
    return render(request, "travel_no.html")

def travel_results(request):
    global country_departure
    global country_arrival
    pays_depart = country_departure
    pays_arrive = country_arrival
    aeroport_depart = str(request.POST["aeroport_depart"])
    aeroport_arrive = str(request.POST["aeroport_arrive"])
    res = get_flights(aeroport_depart,
                 aeroport_arrive,
                 str(request.POST.get("date_depart")),
                 str(request.POST.get("nb_adults"))
                 )
    if res != []:
        dico = {
            'departure': pays_depart,
            'arrival': pays_arrive,
            'aeroport_depart': aeroport_depart,
            'aeroport_arrive': aeroport_arrive,
            'date': get_date(str(request.POST.get("date_depart"))),
            'adults': int(str(request.POST.get("nb_adults"))),
            'flights': {'{}'.format(num): flight for num,flight in zip(range(len(res)),res)}
        }
        return render(request, "travel_results.html",dico)
    return render(request, "travel_no.html")


def a_propos(request):
    return render(request,"a_propos.html")
