from django.conf.urls import url

from . import views 

urlpatterns = [
    url('recherche/$', views.recherche), 
    url('travel/$', views.travel), 
    url('a_propos/$', views.a_propos), 
    url('travel_tmp/$', views.travel_tmp),
    url('travel_results/$', views.travel_results),
    url('travel_no/$', views.travel_no),
    url('welcome/$', views.welcome), 
    url('galerie/$', views.galerie),
    url('visites/$', views.visiter),
    url('restauration/$', views.restauration), 
    url('ajoute_favoris/<int:id>$',views.ville_fav),
    url('supprime_favoris/<int:id>$',views.delete_ville),
    url('supprime_interet/<int:id>$',views.delete_int),
]
