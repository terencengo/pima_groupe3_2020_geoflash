from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class Ville(models.Model):
    

    name = models.CharField(max_length=255)
    favourite = models.ManyToManyField(User,related_name="favourite", blank=True)
    historique = models.ManyToManyField(User,related_name="historique", blank=True)
    
    def __str__(self):
        return self.name
# Create your models here.

