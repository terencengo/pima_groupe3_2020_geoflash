from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Interest(models.Model):
    name = models.CharField(max_length=255)
    user = models.ManyToManyField(User)
    def __str__(self):
        return self.name      


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    interest=models.ManyToManyField(Interest,related_name="interest", blank=True)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


