"""pima_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include, re_path
from pima import views 
from django.conf import settings
from django.conf.urls.static import static
from users import views as user_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('welcome/geoflash/', include('pima.urls')),
    path('geoflash/', include('pima.urls')),
    path('', views.welcome),

    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('profile', auth_views.LoginView.as_view(template_name='users/profile.html'), name='profile'),
    path('password-reset/', 
        auth_views.PasswordResetView.as_view(
            template_name='users/password_reset.html'
        ), 
        name='password_reset'),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='users/password_reset_done.html'
         ),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='users/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='users/password_reset_complete.html'
         ),
         name='password_reset_complete'),
    path('acc-active-sent/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='users/acc_active_sent.html'
         ),
         name='acc_active_sent'),
    path('acc-active-confirm/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='users/acc_active_confirm.html'
         ),
         name='acc_active_confirm'),
    path('acc-active-invalid/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='users/acc_active_invalid.html'
         ),
         name='acc_active_invalid'),
    path('change-password/',
         auth_views.PasswordChangeView.as_view(
             template_name='users/change_password.html'
         ),
         name='change_password'),
    path('password-change-done/',
         auth_views.PasswordChangeDoneView.as_view(
             template_name='users/password_change_done.html'
         ),
         name='password_change_done'),
    path('activate/<uidb64>/<token>/',user_views.activate, name='activate'),
    path('welcome/', views.welcome, name='pima-welcome'),
    path('travel/', views.travel, name='pima-travel'),
    path('travel/travel_results/', views.travel_results, name='pima-travel_results'),
    path('recherche/galerie/', views.galerie, name='pima-galerie'),
    path('profile/', user_views.profile, name='profile'),
    path('recherche/visites/', views.visiter, name='pima-visiter'),
    path('profile/favourites/', views.favourite_list, name='favourite_list'),
    path('profile/historique/', views.historique_list, name='historique_list'),
    path('profile/interest/', views.interest_list, name='interest_list'),
    re_path('ajoute_favoris/(?P<id>\d+)/', views.ville_fav,name='ville_fav'),
    re_path('supprime_favoris/(?P<id>\d+)/', views.delete_ville,name='delete_ville'),
    re_path('supprime_interet/(?P<id>\d+)/', views.delete_int,name='delete_int'),
    path('welcome/',views.welcome,name="retour_accueil"),
    path('travel/',views.travel,name="travel"),
    path('travel/travel_tmp/', views.travel_tmp, name='pima-travel_tmp'),
    path('travel/travel_tmp/travel_results/', views.travel_results, name='pima-travel_results'),
    path('travel/travel_tmp/travel_no/', views.travel_no, name='pima-travel_no'),
    path('travel_results/',views.travel_results,name="travel_results"),
    path('recherche/',views.recherche,name="recherche"),
    path('welcome/',views.retour_accueil,name="retour_accueil"),
    path('recherche/restauration/', views.restauration, name='pima-restauration'),
    path('profile/interest',user_views.addInterest,name='addInterest'),
    path('travel/geoflash/a_propos', views.a_propos, name='pima-a_propos'),
    path('favourites/', views.favourite_list, name='retour_favoris'),
    path('profile/interest/', views.interest_list, name='retour_int'),    
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
