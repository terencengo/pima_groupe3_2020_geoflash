# Groupe n°19

## Réservé au cours

### Composition de l'équipe

| Nom          | Prénom      | Email                         |
| -------------|-------------|-------------------------------|
| NGO| Terence | terence.ngo@ensiie.fr |
| MIR | Nicolas | nicolas.mir@ensiie.fr |
| UTH | Rathea | rathea.uth@ensiie.fr |
| MARY | Loic | loic.mary@ensiie.fr |
| KEYEWA | Aymeric | aymeric.keyewa@ensiie.fr |
| MERCERON | Fabien | fabien.merceron@ensiie.fr |

### Sujet : 2 (Géolocalisation)

### Adresse du backlog : https://trello.com/b/rcJa0qaX/geoflashgroupe3
